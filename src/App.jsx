import { useState, useEffect } from "react";
import HomePage from "./Components/HomePage/HomePage";
import { getAllBoards, addBoard } from "./Authorization/KeyToken";
import "./App.css";

function App() {
  const [boards, setBoards] = useState([]);
  useEffect(() => {
    getAllBoards()
      .then((boards) => {
        setBoards(boards);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const handleAddBoard = (name) => {
    addBoard(name)
      .then((response) => {
        setBoards([...boards, response]);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
    <h1>trello</h1>
      <HomePage boards={boards} handleAddBoard={handleAddBoard} />
    </>
  );
}

export default App;
