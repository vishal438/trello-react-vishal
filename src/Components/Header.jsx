import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import trello from "../assets/Trello-Logo.png";
import { useNavigate } from 'react-router-dom';

function Header() {
    const navigate = useNavigate();

    const imageHandler = () => {
        navigate("/");
    };

    return (
        <AppBar position="static" style={{ background: "white" }}>
            <Toolbar>
                {/* Display the Trello image */}
                <img
                    src={trello}
                    alt="Trello Logo"
                    style={{ width: '80px', height: 'auto', cursor: 'pointer' }}
                    onClick={imageHandler}
                />
            </Toolbar>
        </AppBar>
    );
}

export default Header;
