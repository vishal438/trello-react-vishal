import {
  Paper,
  TextField,
  Button,
  Box,
  Typography,
  Grid,
  Card,
  CardContent,
} from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function Boards(props) {
  const { boards, handleAddBoard } = props;
  const [name, setName] = useState("");
  let navigate = useNavigate();

  function callBoardLists(id) {
    navigate(`boards/${id}`);
  }

  return (
    <Box sx={{ padding: 4 }}>
      <Grid container spacing={3}>
        {boards.map((board) => (
          <Grid item key={board.id} xs={12} sm={6} md={4} lg={3}>
            <Card
              sx={{
                height: "100%",
                backgroundImage: `url(${board.prefs.backgroundImage})`,
                backgroundSize: "cover",
                color: "white",
                backgroundColor: "darkblue",
                cursor: "pointer",
              }}
              onClick={() => callBoardLists(board.id)}
            >
              <CardContent>
                <Typography variant="h6">{board.name}</Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
        {boards.length === 10 ? (
          <Grid item xs={12}>
            <Paper
              sx={{
                p: 2,
                color: "white",
                backgroundColor: "black",
                textAlign: "center",
              }}
            >
              Maximum Board limit Reached
            </Paper>
          </Grid>
        ) : (
          <>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <Paper
                elevation={3}
                sx={{
                  p: 2,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                  columnGap: 2,
                }}
              >
                <TextField
                  fullWidth
                  placeholder="New Board"
                  color="primary"
                  focused
                  value={name}
                  onChange={(event) => {
                    setName(event.target.value);
                  }}
                  InputProps={{
                    style: { color: "black" },
                  }}
                  disabled={boards.length === 10}
                />
                <Button
                  variant="contained"
                  onClick={(event) => {
                    if (name) {
                      handleAddBoard(name);
                      setName("");
                    }
                  }}
                  disabled={boards.length === 10}
                >
                  Add
                </Button>
              </Paper>
            </Grid>
          </>
        )}
      </Grid>
    </Box>
  );
}

export default Boards;
