import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle'
import { getCheckLists, addCheckList, deleteCheckList } from "../../Authorization/KeyToken";
import { TextField } from '@mui/material';
import Checklist from './Checklist/CheckList';

const CheckListContainer = ({ card }) => {
    const [open, setOpen] = useState(false);
    const [checkList, setCheckList] = useState([])
    const [name, setName] = useState('')

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    useEffect(() => {
        getCheckLists(card.id)
            .then((checkList) => {
                setCheckList(checkList);
            })
    }, [card.id]);

    const handleAddCheckList = (name) => {
        addCheckList(name, card.id)
            .then((response) => {
                setCheckList([...checkList, response]);
            })
    };

    const handleDeleteCheckList = (checkListId) => {
        deleteCheckList(card.id, checkListId)
            .then((response) => {
                const updatedCheckList = checkList.filter((checklist) => {
                    return checklist.id !== checkListId
                })
                setCheckList(updatedCheckList);
            })
    };

    return (
        <div>
            <Button onClick={handleClickOpen} sx={{ maxWidth: '160px' }}>{card.name}</Button>
            <Dialog
                open={open}
                onClose={handleClose}
                scroll='paper'
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogTitle id="scroll-dialog-title" sx={{ justifyContent: "space-between", display: 'flex', fontSize: '30px' }}>{card.name}
                    <Button onClick={handleClose}>X</Button></DialogTitle>
                <DialogContent dividers={scroll === 'paper'}>
                    <DialogContent>
                        <TextField
                            size='small'
                            placeholder="New CheckList"
                            color="primary"
                            focused
                            value={name}
                            onChange={(event) => {
                                setName(event.target.value);
                            }}
                            variant="outlined"
                            sx={{ color: 'white' }}
                        />
                        <Button
                            variant="outline-secondary"
                            onClick={(event) => {
                                if (name) {
                                    handleAddCheckList(name);
                                    setName('');
                                }
                            }}
                        >
                            Add New CheckList
                        </Button>
                    </DialogContent>
                    <DialogContent
                        id="scroll-dialog-description"
                        tabIndex={-1}
                    >
                        {checkList.map((checklist) => {
                            return <Checklist checklist={checklist} key={checklist.id} handleDelete={() => handleDeleteCheckList(checklist.id)} />
                        })}
                    </DialogContent>
                </DialogContent>
            </Dialog>
        </div>
    );
}

export default CheckListContainer;
