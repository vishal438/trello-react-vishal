import React from "react";
import { Box, Typography } from "@mui/material";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

const Checklist = ({ checklist, handleDelete }) => {
  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="flex-start"
      marginBottom="10px"
    >
      <Box display="flex" alignItems="center" width="100%">
        <CheckBoxIcon />
        <Typography variant="h5" style={{ marginRight: "10px", width: "80%" }}>
          {checklist.name}
        </Typography>
        <i class="fa-solid fa-delete-left" onClick={handleDelete}></i>
      </Box>
    </Box>
  );
};

export default Checklist;
