import React, { useState, useEffect } from "react";
import { getAllCards, addCard, deleteCard } from "../../Authorization/KeyToken";
import {
  Box,
  TextField,
  Typography,
  IconButton,
  Modal,
  Paper,
} from "@mui/material";
import CheckList from "../CheckLists/CheckListContainer";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";

function CardContainer({ listId }) {
  const [cards, setCards] = useState([]);
  const [name, setName] = useState("");

  useEffect(() => {
    getAllCards(listId).then((response) => {
      setCards(response);
    });
  }, [listId]);

  function handleAddCard() {
    addCard(name, listId).then((response) => {
      setCards((prevCards) => [...prevCards, response]);
      setName("");
    });
  }

  function handleCardDelete(cardId) {
    deleteCard(cardId).then(() => {
      let updatedCardArray = cards.filter((card) => {
        return card.id !== cardId;
      });
      setCards(updatedCardArray);
    });
  }

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: 2,
        }}
      >
        {cards.map((card) => (
          <Paper
            key={card.id}
            elevation={3}
            sx={{
              display: "flex",
              justifyContent: "space-between",
              backgroundColor: "white",
              borderRadius: "5px",
              padding: "10px",
              cursor: "pointer",
              transition: "0.3s",
              "&:hover": {
                backgroundColor: "lightgray",
              },
            }}
          >
            <Box>
              <CheckList card={card} />
            </Box>
            <IconButton
              onClick={() => handleCardDelete(card.id)}
              color="error"
            >
              <DeleteIcon />
            </IconButton>
          </Paper>
        ))}
      </Box>

      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          marginTop: 2,
        }}
      >
        <TextField
          placeholder="Add A Card"
          color="primary"
          focused
          value={name}
          onChange={(event) => {
            setName(event.target.value);
          }}
          sx={{ color: "white", flex: 1 }}
        />
        <IconButton
          onClick={() => {
            if (name) {
              handleAddCard(name);
            }
          }}
          color="primary"
          sx={{ backgroundColor: "blue", color: "white", fontSize: 20 }}
        >
          <AddIcon />
        </IconButton>
      </Box>
    </Box>
  );
}

export default CardContainer;
