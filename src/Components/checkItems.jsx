import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import {
  getCheckItems,
  addCheckItems,
  deleteCheckItems,
  updateItemState,
} from "../../key";
import {
  Box,
  Checkbox,
  FormControlLabel,
  FormGroup,
  LinearProgress,
  TextField,
  Typography,
} from "@mui/material";

function CheckItems({ checklist }) {
  const [checkItems, setCheckItems] = useState([]); // State to store checklist items
  const [name, setName] = useState(""); // State to store the new item's name

  useEffect(() => {
    // Fetch checklist items when the component mounts or when the checklist ID changes
    getCheckItems(checklist.id).then((checkItems) => {
      setCheckItems(checkItems);
    });
  }, [checklist.id]);

  const handleAddCheckItem = (name) => {
    // Add a new checklist item and update the state
    addCheckItems(name, checklist.id).then((response) => {
      setCheckItems([...checkItems, response]);
    });
  };

  const handleDeleteCheckItem = (checkListId, checkItemId) => {
    // Delete a checklist item and update the state
    deleteCheckItems(checkListId, checkItemId).then((response) => {
      let filteredCheckedItems = checkItems.filter((checkItem) => {
        return checkItem.id !== checkItemId;
      });
      setCheckItems(filteredCheckedItems);
    });
  };

  const handleUpdateState = (checkItemId, state) => {
    const newState = state === "incomplete" ? "complete" : "incomplete";

    // Update the state of a checklist item and update the state
    updateItemState(checklist.idCard, checkItemId, newState).then(
      (response) => {
        const updatedItems = checkItems.map((item) =>
          item.id === checkItemId ? { ...item, state: newState } : item
        );
        setCheckItems(updatedItems);
      }
    );
  };

  return (
    <Box>
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Box sx={{ minWidth: 35 }}></Box>
      </Box>
      <FormGroup>
        {checkItems.map((checkitem) => {
          return (
            <Box key={checkitem.id} style={{ display: "flex" }}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checkitem.state === "complete"}
                    onChange={() =>
                      handleUpdateState(checkitem.id, checkitem.state)
                    }
                  />
                }
                label={checkitem.name}
              />
              <i
                style={{ marginTop: 13 }}
                class="fa-solid fa-delete-left"
                onClick={() =>
                  handleDeleteCheckItem(checklist.id, checkitem.id)
                }
              ></i>
            </Box>
          );
        })}
      </FormGroup>
      <br />
      <TextField
        size="small"
        placeholder="Item"
        color="primary"
        focused
        value={name}
        onChange={(event) => {
          setName(event.target.value);
        }}
        variant="outlined"
        sx={{ color: "white" }}
      />
      <Button
        variant="outline-secondary"
        id="button-addon2"
        onClick={(event) => {
          if (name) {
            handleAddCheckItem(name);
            setName("");
          }
        }}
      >
        Add
      </Button>
    </Box>
  );
}

export default CheckItems;
