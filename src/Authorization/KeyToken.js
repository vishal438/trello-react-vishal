import axios from "axios";

const apiKey = `201f4a732d46e232bc3e980d8dfa0543`;
const token = `ATTAb5f9073eda5bb0621ae6432c7bed4b6f148b003a044e25690ed63c7bb5da9c9708E7D6D1`;

export const getAllBoards = () => {
  return axios
    .get(
      `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`
    )
    .then((response) => {
      // console.log(response.data);
      return response.data;
    });
};

export const addBoard = (name) => {
  const addBoardUrl = `https://api.trello.com/1/boards/?name=${name}&key=${apiKey}&token=${token}`;
  return axios.post(addBoardUrl).then((response) => {
    // console.log(response)
    return response.data;
  });
};

export const getLists = (id) => {
  return axios
    .get(
      `https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${token}`
    )
    .then((response) => {
      // console.log(response.data);
      return response.data;
    });
};

export const addList = (id, name) => {
  return axios
    .post(
      `https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${apiKey}&token=${token}`
    )
    .then((response) => {
      return response.data;
    });
};

export const deleteList = (id) => {
  return axios
    .put(
      `https://api.trello.com/1/lists/${id}?closed=true&key=${apiKey}&token=${token}`
    )
    .then((response) => {
      return response.data;
    });
};

export const getAllCards = (listID) => {
  return axios
    .get(
      `https://api.trello.com/1/lists/${listID}/cards?key=${apiKey}&token=${token}`
    )
    .then((response) => {
      return response.data;
    });
};

export const addCard = (name, listId) => {
  return axios
    .post(
      `https://api.trello.com/1/cards?name=${name}&idList=${listId}&key=${apiKey}&token=${token}`
    )
    .then((response) => {
      return response.data;
    });
};

export const deleteCard = (id) => {
  return axios
    .delete(`https://api.trello.com/1/cards/${id}?key=${apiKey}&token=${token}`)
    .then((response) => {
      return response.data;
    });
};

export const getCheckLists = (cardId) => {
  const checkListUrl = `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${token}`;
  return axios.get(checkListUrl).then((response) => response.data);
};

export const addCheckList = (name, cardId) => {
  return axios
    .post(
      `https://api.trello.com/1/checklists?name=${name}&idCard=${cardId}&key=${apiKey}&token=${token}`
    )
    .then((response) => response.data);
};

export const deleteCheckList = (CardId, CheckListId) => {
  return axios
    .delete(
      `https://api.trello.com/1/cards/${CardId}/checklists/${CheckListId}?key=${apiKey}&token=${token}`
    )
    .then((response) => response.data);
};
